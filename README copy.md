
# 実機テスト
Simulator だと、カメラが使用できないため実機で確認にしてください。

$ ionic build
$ ionic cap copy
$ ionic cap sync
$ ionic cap open ios
Xcode が開くので、デバイスを自身の端末に指定して ▶

場合によって、Info.plistに下記が無い場合は適時追加
Key / Value(Valueは適当でOK)

Privacy - Media Library Usage Description           / To Pick Medeis from Library
Privacy - Camera Usage Description                  / To Take Photos and Video
Privacy - Location Always Usage Description         / Always allow Geolocation?
Privacy - Location When In Use Usage Description    / Allow Geolocation?
Privacy - Microphone Usage Description              / To Record Audio With Video
Privacy - Photo Library Additions Usage Description / Store camera photos to camera
Privacy - Photo Library Usage Description           / To Pick Photos from Library

[注意]実機 + iOS 13.3.1 だと、クラッシュする問題あり
https://teratail.com/questions/238259


# tabpage

## tabpage1
写真/アルバムから"写真(PICTURE)"や"動画(VIDEO)"を取得する

### Load Image ボタン
写真/アルバムから"写真(PICTURE)"を取得して、取得した写真を表示させる
ファイルは、base64 形式で 下記のように取得

PfE37O3jSbQvEtrJeJZX0Q0fXYbY21nrvh2Tiwlg83zPs+sWNwoEEAZfL1FH0873utDlXprSliY1Kck4xnazv8Kg+b7PK3zKNmk+u6+19VmOe06uCpzy2ftG1/tdDmTnB/Z5pWkpWla7TXys+X6n8H+GfDnlWupW9zbXM07i4sZraVIYbi8Np/aKRDbtKxahBFcoq4zDe3FurENbpLXJGhRo1G7uUFGzgur/JarXr1utpfBYjNcbVqSpbKd01ON0lronbfXf/ANKteP7x/sK/FOLWLC58FvdG4tr3SW1fQ5JH3fNHBEL21jXcQC6ZuAARkrIBwrGvZyjFKVaeHl7t25wbekY8tlTta904vW9tVp1Px3izL50aksVGPLKTanFKzacpPmvqtdrLt8Uj9E/

### Load Video ボタン
写真/アルバムから"動画(VIDEO)"を取得している
ファイルは、Pathを下記のように取得

file:///private/var/mobile/Containers/Data/PluginKitPlugin/3FD99AEA-C405-4886-A8DD-701F1F882A0E/tmp/trim.564F1BED-E555-428B-8AFD-73C96FDE6DB1.MOV

[課題]
「Application」ではなく、「PluginKitPlugin」となっているため、ライフタイムが死んだあとに参照してるからダメということのようです。



## tabpage2
Capture image:
カメラを起動して写真を撮影。撮影した写真は、Mediaタブの一覧に表示する。
ファイルは端末ではなく、アプリ内のストレージに保存

Record Video:
ビデオカメラを起動して動画を撮影。撮影した動画は、Mediaタブの一覧に表示する。

Record Audio:
オーディオを起動して音声を録音。録音した音声は、Mediaタブの一覧に表示する。

Load multiple:
自身の端末（写真/アルバム）から写真を取得。取得した写真はMediaタブの一覧に表示する。
端末からアプリ内のストレージにデータをコピーしている。
動画ファイルの取得には至っていない。
