import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { TabPage2 } from './tabpage2.component';

describe('TabPage2', () => {
  let component: TabPage2;
  let fixture: ComponentFixture<TabPage2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabPage2],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(TabPage2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
