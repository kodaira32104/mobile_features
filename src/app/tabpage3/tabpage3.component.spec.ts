import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { TabPage3 } from './tabpage3.component';

describe('Tab3Page', () => {
  let component: TabPage3;
  let fixture: ComponentFixture<TabPage3>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabPage3],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(TabPage3);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
