import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';



// ### アプリ内で写真や動画の取得に使用
// Camera : スマホ内の写真や動画にアクセスするため
import { Camera } from '@ionic-native/camera/ngx';

// Image Picker: コミュニティからの要求に応じて、ライブラリからこの画像を使用して複数の画像を選択できる
import { ImagePicker } from '@ionic-native/image-picker/ngx';

// File: メディアをキャプチャした後、それをアプリフォルダーに移動する必要があります。Fileプラグインを使用すると、これらすべての操作を実行できる
import { File } from '@ionic-native/File/ngx';

// Media Capture: 動画、画像、音声のキャプチャに使用するプラグイン。 一部のデバイスでは、Androidで音声が問題になる可能性があるため、レコーダーアプリもインストールしていることを確認する必要がある
import { MediaCapture } from '@ionic-native/media-capture/ngx';

// Media: 簡単な方法でオーディオを再生するために、追加のコントロールを使用してオーディオをさらに適切に管理する他の方法もあり
import { Media } from '@ionic-native/media/ngx';

// Streaming Media: プレーヤーで動画を再生する。HTML5動画タグも機能する
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';

// Photo Viewer: 別のコンポーネントに画像ライトボックスを表示したくないので、このプラグインを使用する
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';





@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    ImagePicker,
    File,
    MediaCapture,
    Media,
    StreamingMedia,
    PhotoViewer
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
