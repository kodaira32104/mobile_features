import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPageComponent } from './tabspage.component';

const routes: Routes = [
  {
    path: 'tabspage',
    component: TabsPageComponent,
    children: [
      {
        path: 'tabpage1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabpage1/tabpage1.module').then(m => m.TabPage1Module)
          }
        ]
      },
      {
        path: 'tabpage2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabpage2/tabpage2.module').then(m => m.TabPage2Module)
          }
        ]
      },
      {
        path: 'tabpage3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tabpage3/tabpage3.module').then(m => m.TabPage3Module)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabspage/tabpage1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabspage/tabpage1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
