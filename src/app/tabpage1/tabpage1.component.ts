import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tabpage1.component.html',
  styleUrls: ['tabpage1.component.scss']
})
export class TabPage1Component {
  public currentImage = null;
  public currentVideo = null;

  constructor(private camera: Camera) { }

  loadPicture() {
    /*
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then(data => {
      this.currentImage = 'data:image/jpeg;base64,' + data;
      console.log(data);
    }, err => {
      // Handle error
      console.log(err)
    });
    */

   const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.NATIVE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
    mediaType: this.camera.MediaType.PICTURE
  }

  this.camera.getPicture(options).then(data => {
    //this.currentImage = 'data:image/jpeg;base64,' + data;
    console.log(data);
  }, err => {
    // Handle error
    console.log(err)
  });
  

  }

  loadVideo() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.VIDEO
    }

    this.camera.getPicture(options).then(data => {

      this.currentVideo = data;
      console.log(data);

    }, err => {
      // Handle error
      console.log(err)
    });

  }

}
