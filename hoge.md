

ionic を使用したデモプログラムの進捗共有いたします。
目的としては、iPhoneの写真/アルバムに保存されている動画を取得して、
富士通側に処理を投げたいと思っています。

Git
https://gitlab.com/kodaira32104/mobile_features


動画を取得している画面
動画のPathを一応取得できています。


画像を取得している画面
base64 のファイル形式で取得できています。
※富士通側もbase64みたいなので、これは画像verだが、動画も画像と同じ形で取得したらそのままの形式で渡せる？




現時点の課題として..
現在、動画のPathは取得できていますが、
プレイヤーで再生できない(操作できない)ので確認を進めています。

今、一番の原因として考えられる箇所は、
下記にあるように、取得しているPathが微妙に違っていることが原因として考えられます。

①Media動画（動画再生可）
file:///private/var/mobile/Containers/Data/Application/A00AD0CC-247C-4574-A61A-2CF3AB74A55D/tmp/519E27E3-BF00-4132-BD79-136A6E691DF3.MOV
※ [Media]タブのSelectMedia＞Record Videoで撮影した場合のファイルパス
※ 端末（写真/アルバム）に保存ではなく、アプリ内のストレージに一度コピーしている

②Album動画（動画再生不可）
file:///private/var/mobile/Containers/Data/PluginKitPlugin/3FD99AEA-C405-4886-A8DD-701F1F882A0E/tmp/trim.564F1BED-E555-428B-8AFD-73C96FDE6DB1.MOV
※ [Album]タブのLoadVideoボタンを押して取得した場合のファイルパス
※ 端末（写真/アルバム）に保存されているデータを直接取得している

Album動画は、「Application」ではなく「PluginKitPlugin」となっているため、
ライフタイムが死んだあとに参照してるからダメということのようです。
https://stackoverflow.com/questions/57798968/didfinishpickingmediawithinfo-returns-different-url-in-ios-13


正常な？Pathを取得できれば、富士通側にデータを送付できると考えています。




FILE_URI
file:///private/var/mobile/Containers/Data/PluginKitPlugin/AF6A17B7-1FC0-4C2C-98CD-DD415F833C97/tmp/trim.E965654B-5C5B-4B53-B314-F80620B77A24.MOV

DATA_URL
file:///private/var/mobile/Containers/Data/PluginKitPlugin/AF6A17B7-1FC0-4C2C-98CD-DD415F833C97/tmp/trim.4ABECA7A-4735-4B5F-AB25-1768D5C97AE5.MOV

NATIVE_URI
file:///private/var/mobile/Containers/Data/PluginKitPlugin/AF6A17B7-1FC0-4C2C-98CD-DD415F833C97/tmp/trim.60CBFB2B-EE09-4570-9D68-8A74A5A50B75.MOV


FILE_URI
file:///var/mobile/Containers/Data/Application/A5194449-F6F9-4302-AB0D-2E467BBAE1DF/tmp/cdv_photo_011.jpg
file:///var/mobile/Containers/Data/Application/A5194449-F6F9-4302-AB0D-2E467BBAE1DF/tmp/cdv_photo_012.jpg

NATIVE_URI
assets-library://asset/asset.PNG?id=EC8D1168-2852-4B72-BA95-048E91CDD328&ext=PNG
assets-library://asset/asset.PNG?id=EC8D1168-2852-4B72-BA95-048E91CDD328&ext=PNG
